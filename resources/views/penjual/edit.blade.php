@extends('sayursegar.master')
@section('judul')
show penjual
@endsection

@section('content')

<div class="container mt-5 p 5">
    
    <form action="/penjual/{{$penjual->id}}" method="POST">
        
        @csrf
        @method('PUT')
       
        <div class="form-group">
            <label for="title">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{$penjual->nama}}" id="title" placeholder="Masukkan Title">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Alamat</label><br>
            <textarea name="alamat" class="form-control" cols="10" rows="1">{{$penjual->alamat}}"</textarea>
            @error('alamat')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>No Hp</label><br>
            <input type="number" class="form-control" name="no_hp"  value="{{$penjual->no_hp}}" placeholder="NO handphone">
            @error('no_hp')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>


    
@endsection