@extends('layout.master')

@section('judul')
    Tambah Data Penjual
@endsection

@section('content')
    




<div>
   
        <form action="/penjual" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama Penjual</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan Nama Penjual">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>Alamat</label><br>
               <textarea name="alamat" class="form-control" placeholder="Masukan alamat" cols="30" rows="1"></textarea>
                @error('alamat')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <div class="form-group">
                <label>No Hp</label>
                <input type="text" class="form-control" name="no_hp"  placeholder="Masukkan Nama Penjual">
                @error('no_hp')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection