@extends('sayursegar.master')

@section('judul')
    Tambah Data kategori
@endsection

@section('content')

<div>
   
        <form action="/kategori" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama kategori</label>
                <input type="text" class="form-control" name="nama_kategori"  placeholder="Masukkan Nama Penjual">
                @error('nama_kategori')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection