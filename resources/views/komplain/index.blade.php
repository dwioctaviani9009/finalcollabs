@extends('sayursegar.master')

@section('judul')
    KRITIK DAN SARAN
@endsection

@section('content')
 <h2 class="col-8 m-5"> Halaman Komplain</h2>

<div class="col-8 my-10 m-5">
<a href="/komplain/create" class="btn btn-primary my-3">Tambah</a><br>


        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">No</th>
                <th scope="col">ID Pembeli</th>
                <th scope="col">ID Penjual</th>
                <th scope="col">Nama Sayur </th>
                <th scope="col">Keterangan</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($komplain as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->pembeli_id}}</td>
                        <td>{{$value->penjual_id}}</td>
                        <td>{{$value->nama_sayuran}}</td>
                        <td>{{$value->keterangan}}</td>
                        <td>
                            <form action="/posts/{{$value->id}}" method="POST">
                            <a href="/posts/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="/posts/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
    </div>
@endsection