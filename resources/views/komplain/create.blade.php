@extends('layout.master')

@section('judul')
    KRITIK DAN SARAN
@endsection

@section('content')



   <div>
    <form action="/komplain" enctype="multipart/form-data" method="POST">
        @csrf
        <div class="form-group">
            <label>Pembeli</label>
            
           <select class="form-control" name="pembeli_id">
            <option value="">--pilih Pembeli--</option>
                @foreach ($pembeli as $b)
                    <option value="{{$b->id}}">{{$b->nama_pembeli}}</option>
                @endforeach
           </select>
            @error('pembeli_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Penjual</label>
           <select class="form-control" name="penjual_id" >
            <option value="">--pilih Penjual--</option>
                @foreach ($penjual as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endforeach
           </select>
            @error('penjual_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <div class="form-group">
            <label>Nama Sayur</label>
            <input type="text" class="form-control" name="sayuran"  placeholder="Masukkan Nama Sayuran">
            @error('sayuran')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>Keterangan</label><br>
           <textarea name="keterangan" class="form-control" placeholder="Masukan alamat" cols="30" rows="1"></textarea>
            @error('keterangan')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        
        <button type="submit" class="btn btn-primary">pesan</button>
    </form>



@endsection