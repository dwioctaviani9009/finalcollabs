    <!-- Humberger Begin -->
    <div class="humberger__menu__overlay"></div>
    <div class="humberger__menu__wrapper">
        <div class="humberger__menu__logo">
            <a href="#"><img src="{{asset('/tokosayur/img/logo.png')}}" alt=""></a>
        </div>
        <div class="humberger__menu__widget">
            <div class="header__top__right__auth">
                <a href="/login"><i class="fa fa-user mb-3"></i> Login</a>
                <a href="/register"><i class="fa fa-user"></i> Register</a>
            </div>
        </div>
        <nav class="humberger__menu__nav mobile-menu">
            <ul>
                <li class=""><a href="/">Home</a></li>
                <li><a href="/about">about</a></li>
                <li><a href="#">Products</a>
                    <ul class="header__menu__dropdown">
                        <li><a href="./shop-details.html">Sayuran</a></li>
                        <li><a href="./shoping-cart.html">Buah-buahan</a></li>
                    </ul>
                </li>
                <li><a href="./blog.html">Artikel</a></li>
                <li><a href="./contact.html">Contact</a></li>
            </ul>
        </nav>
        <div id="mobile-menu-wrap"></div>
        <div class="header__top__right__social">
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-linkedin"></i></a>
            <a href="#"><i class="fa fa-pinterest-p"></i></a>
        </div>
        <div class="humberger__menu__contact">
            <ul>
                <li><i class="fa fa-envelope"></i> sayursegar@gmail.com</li>
                <li>Gratis ongkir pulau jawa</li>
            </ul>
        </div>
    </div>
    <!-- Humberger End -->


<!-- Header Section Begin -->
    <header class="header bg-white">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    {{-- <div class="col-lg-3">
                        {{-- <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> sayursegar@gmail.com</li>
                            </ul>
                        </div> --}}
                    {{-- </div>  --}}
                    {{-- <div class="col-lg-9 mt-2 d-flex justify-content-end">
                        <a href="/login"><button type="button" class="btn btn-outline-success btn-sm mr-2">Login</button></a>
                        <a href="/register"><button type="button" class="btn btn-outline-success btn-sm">Register</button></a>
                    </div> --}}
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/"><img src="{{ asset('/tokosayur/img/logo.png')}}" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-10">
                    <nav class="header__menu">
                        <ul>
                            <li><a href="/">Home</a></li>
<<<<<<< HEAD
                            <li><a href="/transaksi">Transaksi</a></li>
=======
                            <li><a href="/about">About</a></li>
>>>>>>> 03c1afa7a9eeeff7069ff2913413a9f3211f76ce
                            <li><a href="#">Products</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="./shop-details.html">Sayuran</a></li>
                                    <li><a href="./shoping-cart.html">Buah-buahan</a></li>
                                </ul>
                            </li>
                            <li><a href="/penjual">Penjual</a></li>
                            <li><a href="/pembeli">Pembeli</a></li>
                            <li><a href="/komplain">Komplain</a></li>
                            <li><a href="/sayur">sayur</a></li>
                            <li><a href="/kategori">kategori</a></li>
                       
                            <li >
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                    </nav>
                </div>             
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Header Section End -->