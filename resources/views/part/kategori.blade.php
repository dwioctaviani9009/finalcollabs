    <!-- Categories Section Begin -->
    <section class="categories">
        <div class="container">
            <div class="row">
                <div class="categories__slider owl-carousel">
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{ asset ('/tokosayur/img/categories/sayurdaun.png')}}">
                            <h5><a href="#">Sayuran Daun</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{ asset ('/tokosayur/img/categories/sayurbatang.png')}}">
                            <h5><a href="#">Sayuran Batang</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{ asset ('/tokosayur/img/categories/sayurbuah.png')}}">
                            <h5><a href="#">Sayuran Buah</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{ asset ('/tokosayur/img/categories/sayurjamur.png')}}">
                            <h5><a href="#">Sayuran Jamur</a></h5>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="categories__item set-bg" data-setbg="{{ asset ('/tokosayur/img/categories/sayurpolong.png')}}">
                            <h5><a href="#">Sayuran Polong</a></h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Categories Section End -->