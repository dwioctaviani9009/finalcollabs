@extends('sayursegar.master')

@section('judul')
    Tambah Data Penjual
@endsection

@section('content')
    
Tambah Data Penjual



<div>
   
        <form action="/sayur" method="POST">
            @csrf
            <div class="form-group">
                <label>Nama sayur</label>
                <input type="text" class="form-control" name="nama"  placeholder="Masukkan nama sayur">
                @error('nama')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Harga</label>
                <input type="text" class="form-control" name="harga"  placeholder="harga jual">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>stock</label>
                <input type="text" class="form-control" name="stok"  placeholder="stock">
                @error('stok')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>            
            

            <div class="form-group">
                            {{-- <select name="penjual_id" class="form-control @error('penjual_id') is-invalid @enderror"> --}}
                                <label >Penjual</label>
                                <select class="form-control" name="penjual_id" >
                                @foreach ($penjual as $jual)
                                <option value="{{$jual->id}}">{{$jual->nama}}</option>
                                @endforeach
                            </select>
                            @error('penjual_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
            </div>
            <div class="form-group">
                            <select name="kategori_id" class="form-control @error('kategori_id') is-invalid @enderror">
                                @foreach ($kategori as $item)
                                <option value="{{$item->id}}">{{$item->nama}}</option>
                                @endforeach
                            </select>
                            @error('kategori_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
            </div>


       <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
</div>

@endsection