@extends('sayursegar.master')

@section('judul')
    Halaman Sayur
@endsection

@section('content')

<a href="/sayur/create" class="btn btn-primary">Tambah</a>
<table class="table">
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama sayur</th>
        <th scope="col">Harge</th>
        <th scope="col">Stock</th>
        <th scope="col">Kategori</th>
        <th scope="col">Nama penjual</th>
      </tr> 
    </thead>
    <tbody>
        @forelse ($sayuran as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->harga}}</td>
                <td>{{$value->stock}}</td>
                <td>{{$value->kategori->nama}}</td>
                <td>{{$value->penjual->nama}}</td>
                <td>
                    <form action="/posts/{{$value->id}}" method="POST">
                    <a href="/posts/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/posts/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
@endsection