@extends('sayursegar.master')

@section('content')
    <!-- Hero Section Begin -->
    <section class="hero">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="hero__categories">
                        <div class="hero__categories__all">
                            <i class="fa fa-bars"></i>
                            <span>Kategori Pilihan</span>
                        </div>
                        <ul>
                            <li><a href="#">Sayuran</a></li>
                            <li><a href="#">Buah-buahan</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="hero__search">
                        <div class="hero__search__form">
                            <form action="#">
                                <input type="text" placeholder="Cari yang segar disini">
                                <button type="submit" class="site-btn">CARI</button>
                            </form>
                        </div>
                        <div class="hero__search__phone">
                            <div class="hero__search__phone__icon">
                                <i class="fa fa-phone"></i>
                            </div>
                            <div class="hero__search__phone__text">
                                <h5>0899-77-97-111</h5>
                                <span>Call Center 24jam</span>
                            </div>
                        </div>
                    </div>
                    <div class="hero__item set-bg">
                        {{-- SIMPAN KONTEN DATABASE DISINI --}}

                        {{-- AKHIR KONTEN DATABASE DISINI --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hero Section End -->
@endsection