<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/style.css')}}" type="text/css">

    <title>Register</title>
  </head>
  <body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <div class="container">
        <div class="d-flex justify-content-center">
            <a href="/"><img src="{{ asset('/tokosayur/img/logo.png')}}" class="img-fluid " alt="..."></a>
        </div>
    </div>
        <br><br><br>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="d-flex justify-content-start mr-3">
                    <img src="{{ asset('/tokosayur/img/regimage.png')}}" class="img-fluid" alt="...">
                </div>
                <div class="text-center">
                    <h4>Belanja Sayuran Segar Hanya di Sayur Segar</h4>
                    <h6>Gabung dan rasakan kemudahan bertransaksi di Sayur Segar</h6>
                </div>
            </div>
            <div class="col-lg-6">
                <form class="row g-3">
                    <div class="col-10 mt-3">
                      <label for="inputEmail4" class="form-label">Nama</label>
                      <input type="email" class="form-control" id="inputEmail4" placeholder="">
                    </div>
                    <br>
                    <div class="col-10 mt-3">
                      <label for="inputAddress" class="form-label">Email</label>
                      <input type="text" class="form-control" id="inputAddress" placeholder="">
                    </div>
                    <div class="col-10 mt-3">
                      <label for="inputAddress2" class="form-label">Password</label>
                      <input type="text" class="form-control" id="inputAddress2" placeholder="">
                    </div>
                    <div class="col-10 mt-3">
                      <button type="submit" class="btn btn-primary">Sign in</button>
                    </div>
                    <div class="col-10 mt-3">
                      <p>Sudah punya akun Sayur Segar? <a href="/login" class="btn btn-outline-secondary">  Masuk</a></p>
                    </div>
                  </form>
            </div>
        </div>
    </div>

    <br><br>

    @include('part.footer')


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

        <!-- Js Plugins -->
        <script src="{{ asset('/tokosayur/js/jquery-3.3.1.min.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/jquery-ui.min.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/jquery.slicknav.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/mixitup.min.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/owl.carousel.min.js')}}"></script>
        <script src="{{ asset('/tokosayur/js/main.js')}}"></script>

  </body>
</html>




































