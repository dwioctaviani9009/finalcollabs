@extends('sayursegar.master')

@section('judul')
    Halaman Penjual
@endsection

@section('content')
<h2 class="col-8 m-5"> Halaman Pembeli</h2>
<div class="container mt-5 p 5">
<a href="/pembeli/create" class="btn btn-primary my-3">Tambah</a><br>
<table class="table">
 
    <thead class="thead-light">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama Pembeli</th>
        <th scope="col">Alamat</th>
        <th scope="col">No Hp</th>
        <th scope="col">Actions</th>
      </tr>
    </thead>
    <tbody>
        @forelse ($pembeli as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama_pembeli}}</td>
                <td>{{$value->alamat}}</td>
                <td>{{$value->no_hp}}</td>
                <td>
                    
                    <form action="/pembeli/{{$value->id}}" method="POST">
                        @csrf
                        @method('DELETE')

                    <a href="/pembeli/{{$value->id}}" class="btn btn-info">Show</a>
                    <a href="/pembeli/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                    
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>
</div>
@endsection