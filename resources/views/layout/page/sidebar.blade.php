<aside class="main-sidebar sidebar-dark-primary">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <img src="admin/dist/img/sayur.jpg"  class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Toko Sayuran Online</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar dark-link">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="info">
        <a href="#" class="d-block"></a>

      </div>
    </div>

    <!-- SidebarSearch Form -->
    

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column mb-3 d-flex" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav primary-link ">
          <a href="/home" class="nav-link user-panel mt-0 pb-3 mb-1 d-flex">
           
             <i class="nav-icon fas fa-tachometer-alt">
            
              Halaman Utama
              
          
            </i> 
          </a>
        </li>
        <li class="nav-item">
          <a href="/penjual" class="nav-link">
              <i class="fa fa-table" aria-hidden="true"></i>
               <p>
                Penjual
              
              </p>
                         
          </a>
        </li>
        <li class="nav-item">
          <a href="/pembeli" class="nav-link">
              <i class="fa fa-table" aria-hidden="true"></i>
               <p>
                Pembeli
                
              </p>
                         
          </a>
        </li>

        <li class="nav-item">
          <a href="/komplain" class="nav-link">
              <i class="fa fa-table" aria-hidden="true"></i>
               <p>
                Komplain
              </p>
           </a>
       </li>
       

      </ul>

    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside> 