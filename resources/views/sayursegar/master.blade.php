<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Ogani Template">
    <meta name="keywords" content="Ogani, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="icon" href="{{ asset('/tokosayur/img/logo.png')}}"">
    <title>Toko Segar</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/jquery-ui.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ asset('/tokosayur/css/style.css')}}" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('part.navbar')
    <!-- Header Section End -->

    <!-- Hero Section Begin -->
    @yield('content')
    <!-- Hero Section End -->

    <!-- Categories Section Begin -->
    {{-- @include('part.kategori') --}}
    <!-- Categories Section End -->

    <!-- Blog Section Begin -->
    {{-- @include('part.blog') --}}
    <!-- Blog Section End -->

    <!-- Footer Section Begin -->
    @include('part.footer')
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="{{ asset('/tokosayur/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/jquery.slicknav.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/mixitup.min.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/owl.carousel.min.js')}}"></script>
    <script src="{{ asset('/tokosayur/js/main.js')}}"></script>



</body>

</html>