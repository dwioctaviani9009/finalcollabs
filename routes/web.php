<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ROUTE DIKA
// Route::get('/', function () {
//     return view('pages.home');
// });

// Route::get('/about', function () {
//     return view('pages.about');
// });

// Route::get('/register', function () {
//     return view('pages.register');
// });

// Route::get('/login', function () {
//     return view('pages.login');
// });

// Route::get('/penjual', function () {
//     return view('pages.penjual');
// });

// Route::get('/pembeli', function () {
//     return view('pages.pembeli');
// });
// // ROUTE DIKA


// Route::get('/', function ('index') {
//     return view('layout.master');
// });



// Route::get('/home', 'HomeController@master');








Route::group(['middleware' => ['auth']], function () {
    //
    Route::get('/', function(){
        return view('home');
    });

Route::get('/penjual','PenjualController@index');
Route::get('/penjual/create','PenjualController@create');
Route::post('/penjual','PenjualController@store');
Route::get('/penjual/{penjual_id}','PenjualController@show');
Route::get('/penjual/{penjual_id}/edit','PenjualController@edit');
Route::put('/penjual/{penjual_id}','PenjualController@update');
Route::delete('/penjual/{penjual_id}','PenjualController@destroy');






Route::get('/sayur','SayurController@index');
Route::get('/sayur/create','SayurController@create');
Route::post('/sayur','SayurController@store');

Route::get('/kategori','KategoriController@index');
Route::get('/kategori/create','KategoriController@create');
Route::post('/kategori','KategoriController@store');






// Route::get('/komplain','komplainController@index');
// Route::get('/komplain/create','komplainController@create');
Route::resource('komplain','KomplainController');



Route::resource('pembeli','PembeliController');

});

Auth::routes();