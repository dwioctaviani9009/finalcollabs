<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\komplain;
use App\penjual;
use App\pembeli;
class KomplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // // $komplain = DB::table('komplain')->get();
        // $penjual = penjual::all();
        // return view('komplain.index', compact('penjual','komplain'))
        $komplain = DB::table('komplain')->get();
        return view('komplain.index', compact('komplain'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pembeli = pembeli::all();
        $penjual = penjual::all();
        return view('komplain.create',compact('penjual','pembeli'));

        
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)

    {   
         $request->validate([
        'pembeli_id' => 'required|unique:pembeli',
        'penjual' => 'required|unique:penjual',
        'nama_sayuran' => 'required',
        'keterangan' => 'required'
    ]);
    
    $query = DB::table('penjual')->insert([
        "pembeli_id" => $request["pembeli_id"],
        "penjual_id" => $request["penjual_id"],
        "nama_sayuran" => $request["nama_sayuran"],
        "keterangan" => $request["keterangan"],
    ]);

               
            return redirect('/komplain');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pembeli = DB::table('pembeli')->where('id', $id)->first();
        $komplain = DB::table('komplain')->where('id', $id)->first();
        return view('komplain.show', compact('komplain','pembeli'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
