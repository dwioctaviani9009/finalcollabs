<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PenjualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $penjual = DB::table('penjual')->get();
        return view('penjual.index', compact('penjual'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('penjual.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|unique:penjual',
            'alamat' => 'required',
            'no_hp' => 'required'
        ]);
        $query = DB::table('penjual')->insert([
            "nama" => $request["nama"],
            "alamat" => $request["alamat"],
            "no_hp" => $request["no_hp"]
        ]);

        return redirect('/penjual');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $penjual = DB::table('penjual')->where('id', $id)->first();
        return view('penjual.show', compact('penjual'));
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penjual = DB::table('penjual')->where('id', $id)->first();
        return view('penjual.edit', compact('penjual'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required|unique:penjual',
            'alamat' => 'required',
            'no_hp' => 'required',
        ]);
       
        $query = DB::table('penjual')
        ->where('id', $id)
        ->update([
            "nama" => $request["nama"],
            "alamat" => $request["alamat"],
            "no_hp" => $request["no_hp"]

        ]);

    //    SET nama =$request['nama'],alamat =$request['alamat'],no_hp=$request['no_hp']
    //         where id=$id
            
        
        return redirect('/penjual');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('penjual')->where('id', $id)->delete();
        return redirect('/penjual');

    }
}
