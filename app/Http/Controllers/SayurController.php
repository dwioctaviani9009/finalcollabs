<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class SayurController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
<<<<<<< HEAD
        $sayuran = Sayur::all();
        return view('sayur.index',compact('sayuran'));
=======
        $sayuran = DB::table('sayuran')->get();
        return view('sayur.index', compact('sayuran'));
>>>>>>> 03c1afa7a9eeeff7069ff2913413a9f3211f76ce

        // $index = Sayur::paginate(20);
        // return view('index', compact('index'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('sayur.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // request()->validate([
        //     'nama' => 'required',
        //     'harga' => 'required',
        //     'stock' => 'required',
        //     'penjual_id' => 'required',
        //     'kategori_id' => 'required'
        // ]);

        // $form = new Sayur;
        // $form->nama = request()->nama;
        // $form->harga = request()->harga;
        // $form->stock = request()->stock;
        // $form->penjual_id = request()->penjual_id;
        // $form->kategori_id = request()->kategori_id;
        // $form->save();
        $request->validate([
            'nama' => 'required|unique:penjual',
            'harga' => 'required',
            'stock' => 'required',
            'penjual_id' => 'required',
            'pembeli_id' => 'required'

        ]);
        $query = DB::table('sayuran')->insert([
            "nama" => $request["nama"],
            "harga" => $request["harga"],
            "stock" => $request["stock"],
            "penjual_id"=> $request["penjual_id"],
            "pembeli_id"=> $request["pembeli_id"]
        ]);

        // return redirect('/penjual');


        return redirect('/sayur');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $from = Syaur::find($id);
        return view('', compact(''), compact(''));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sayur = DB::table('sayuran')->where('id', $id)->first();
        return view('sayur.edit', compact('sayur'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'nama' => 'required',
            'harga' => 'required',
            'stock' => 'required',
            'penjual_id' => 'required',
            'kategori_id' => 'required'
        ]);

        $form = new Sayur;
        $form->nama = request()->nama;
        $form->harga = request()->harga;
        $form->stock = request()->stock;
        $form->penjual_id = request()->penjual_id;
        $form->kategori_id = request()->kategori_id;
        $form->save();

        return redirect('/sayur');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $query = DB::table('sayuran')->where('id', $id)->delete();
        return redirect('/sayur');

    }
}
