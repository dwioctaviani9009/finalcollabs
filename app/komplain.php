<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\pembeli;
class komplain extends Model
{
    protected $table='komplain';

    protected $fillable = ['pembeli_id','penjual_id','nama_sayuran','keterangan'];

    // public function penjual(){
    //     return $this->belongsTo('App\komplain');
    // }
}
